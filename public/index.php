<?php require_once "../includes/databases_conn.php"; ?>
<?php require_once "../includes/functions.php"; ?>

<!DOCTYPE html>
<html>
<head>
    <title>Remembering Jacob George</title>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/custom.css"  media="screen,projection"/>
      <link href='https://fonts.googleapis.com/css?family=Rancho' rel='stylesheet' type='text/css'>
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

      <script src='https://www.google.com/recaptcha/api.js'></script>
      <script type="text/javascript" src="js/custom.js"></script>

      <?php include 'modules/opengraph.php'; ?>

</head>
<body>
       <div class="parallax-container valign-wrapper center-align">
          <div class="parallax"><img src="images/bg/01.jpg"></div>
          <div class="row">
            <div class="col s12">
              <div class="left-align responsive-img"><img class="logo" src="images/logo.png"></div>
            </div>
          </div>
        </div>
<!--

.....


 Really daaa 

-->

        <div class="section white">
          <div class="row container">
            <p class="grey-text text-darken-3 lighten-3 custom-p">
              It is very beautiful to have fun alongside our friends but there are times when we have to say goodbye to them with a lot of grief. Always, in our lifetime a friend leaves this world and we have to be strong to withstand this.
              Clearly we are not the only ones who suffer, their loved ones and those who are closest to them experience it a great deal.
            </p>
            <p class="grey-text text-darken-3 lighten-3 custom-p">
              Jacob, you always stood out of all people by your cute character and your big heart. Those of us who got to know you were very  lucky for having crossed paths with you, now you have God by your side.<br>
              We know that we will not find someone like you again, so happy, so motivating, in good times and in bad times you were always drawing a smile and were there to support us.
              Right now the sorrow fills our heart, but now that you have departed we cannot stop bringing up your name, you will be missed for all that you taught us and for all those beautiful moments that you made us live with you.
              Now that you are in the glory of the Lord, we know that you are very peaceful because here on earth you always did good and you strived to be happy every day of your life.
            </p>
            <p class="grey-text text-darken-3 lighten-3 custom-p">
             We all miss u very very badly. But we all know that you have just departed to another life, we will remember you at every moment as you were an exceptional person, however we say farewell, knowing you will take care of our paths here on earth. 
             <br>
             <br>We would like to honor you by sharing the memories we had with you, mate. So that those memories will live in this whole wide web, forever.
             <em>Chackochi, we all love you</em>            
            </p>
            
          </div>
        </div>
        <div class="parallax-container">
          <div class="parallax"><img src="images/bg/02.jpg"></div>
        </div>

        <div class="section white">
          <div class="row container">
            <h2 id="write" class="scrollspy">Share your Reminiscence</h2>
          </div>
          <div class="row container">
            <form class="col s12" action="create_pyr.php" method="POST" name="myForm" onsubmit="return(validate());">
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="Name" id="name" name="Name" type="text" class="validate">
                  <label for="name">Name</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input id="email" placeholder="Email" name="EMail" type="email" class="validate">
                  <label for="email">Email (Will not be published)</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input id="relationship" placeholder="Relationship with Jacob" name="RElation" type="text" class="validate">
                  <label for="email">Relationship with Jacob</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                    <textarea id="textarea1" name="MEssage" placeholder="What's in your mind?" class="materialize-textarea"></textarea>
                    <label for="last_name">Whats In your Mind?</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                    <div class="g-recaptcha" data-sitekey="6Lfpcg4TAAAAALO3pYspkNY1Hg2Vds2R05WTAZ5l"></div>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                    <input type="submit" class="btn red" value="SHARE" name="submit">
                </div>
              </div>
            </form>
          </div>
        </div>


        <div class="section custborder white">
          <div class="row container">
                <?php while ($row = mysqli_fetch_assoc($result)) { ?>
                  <div class="card scrollspy" <?php  echo 'id="'.$row["name"].'"' ?> >
                    <div class="row card-content">
                      <div class="col s10">
                          <div class="card-title red-text"><?php echo $row["name"]. "<br>"; ?></div>
                          <div class="red-text relation"><?php echo $row["relation"]. "<br>"; ?></div>
                          <?php echo $row["message"]. "<br>";  ?>
                      </div>
                      <div class="col s2">

                        <?php
                          $email = $row["email"];
                          $default = "http://showdown.gg/wp-content/uploads/2014/05/default-user.png";
                          $size = 100;
                          $grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
                        ?>
                         <img src="<?php echo $grav_url; ?>" class="responsive-img circle" width="100px" height="100px">
                      </div>
                    </div>
                  </div>
                  <?php  } ?>

                  <?php
                    // 4. Release returned data
                    mysqli_free_result($result);
                  ?>
          </div>
        </div>



        <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
          <a class="btn-floating btn-large waves-effect waves-light red" href="#write"><i class="material-icons">add</i></a>
        </div>

      <footer class="footerbg white-text">
      <?php if (isset($connection)) { mysqli_close($connection); }?>
        <div class="section container">
         <div class="row">
          <div class="col l4 m4 s12">
           <p>&copy; 2015 |<a target="_blank" href="http://jacobgeorge.in"> Remembering Jacob George aka Chakochi</a> <br> Built out of love, tears and pain</p>
           <img class="responsive-img" src="images/favicon.png" width="100px">
           <p>"When a great man dies, for years the light he leaves behind him, lies on the path of men."</p>
          </div>
          <div class="col s12 m8 l8">
           <div class="row">
             <div class="col l3">
              <a href="http://jyothisjoy.com/" target="_blank"><img class="responsive-image circle" src="images/makers/joe.jpg" width="100px"></a>
              <h5>Jyothis Joy</h5>
              <p>Developer</p>
             </div>
             <div class="col l3">
              <a href="https://www.facebook.com/basil.bose3?fref=ts" target="_blank"><img class="responsive-image circle" src="images/makers/basil.jpg" width="100px"></a>
              <h5>Basil Bose</h5>
              <p>Designer</p>
             </div>
             <div class="col l3">
               <a href="https://www.facebook.com/kiran.joshy.14?fref=ts" target="_blank"><img class="responsive-image circle" src="images/makers/kiran.jpg" width="100px"></a>
               <h5>Kiran Joshy</h5>
               <p>Developer</p>
             </div>
             <div class="col l3">
               <a href="https://www.facebook.com/meera.biju.39?fref=ts" target="_blank"><img class="responsive-image circle" src="images/makers/meera.jpg" width="100px"></a>
               <h5>Meera Biju</h5>
               <p>Content</p>
             </div>
           </div>
          </div>
         </div>
         <div class="row">
           <h5>Usage Policy</h5>
           <p>This website is built in memory of Jacob George. Anyone in the whole world wide web can use it. Best part, all this code is Open Source.<br>Hack and contribute <a target="_blank" href="https://bitbucket.org/jyothisjoy/jacob-george/">here.</a></p>
         </div>
         <div class="row">
           <h5>Privacy</h5>
           <p>This website collect email id for retrieving pictures using gravatar. You will not receive mail from this website
           <br> We hate spams as much as you do.<br>Want your image to show up? Register your email ID at <a target="_blank" href="http://gravatar.com">Globaly Recognized Avatars(Gravatar)</a></p>
         </div>
        </div>
      </footer>
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>

      <script type="text/javascript">

        $(document).ready(function(){
         $('.parallax').parallax();});


        $(document).ready(function(){
         $('.scrollspy').scrollSpy();});


      </script>


</body>
</html>
