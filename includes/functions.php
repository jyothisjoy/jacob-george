<?php
	
	function redirect_to($new_location)
	{
		header("Location: " . $new_location);
		exit;
	}

	// 2. Perform database query
	$query  = "SELECT * FROM messages";
	$result = mysqli_query($connection, $query);
	// Test if there was a query error
	if (!$result) {
		die("Database query failed.");
	}
?>